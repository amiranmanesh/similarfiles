#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctime>
#include <windows.h>


using namespace std;

vector<off_t> subFileSize;
vector<string> subFileTime;
vector<string> subFileName;
int allFilesRead = 0;

bool list_dir(string aa) {
	subFileSize.clear();
	subFileTime.clear();
	subFileName.clear();
	string a = aa + "\\";
	const char *path = a.data();
	int allFiles = 0;
	int simlirarFiles = 0;
	struct dirent *entry;
	DIR *dir = opendir(path);

	if (dir == NULL) {
		return 0;
	}
	while ((entry = readdir(dir)) != NULL) {
		string temps = a + entry->d_name;
		struct stat fileInfo;
		stat(temps.data(),&fileInfo);
		off_t sizee = fileInfo.st_size;
		string time = std::ctime(&fileInfo.st_mtime);
		for (int i = 0; i < subFileName.size(); ++i) {
			if (subFileSize[i] == sizee && subFileTime[i] == time && subFileName[i] != temps) {
			    cout<<"***** similar:(file size and last modified time)\n";
                    cout<<"1) "<<temps<<"\n2) "<<subFileName[i]<<endl;
            cout<<"*****\n";
				simlirarFiles++;
			}
		}
		subFileSize.push_back(sizee);
		subFileTime.push_back(time);
		subFileName.push_back(temps);

		allFiles++;
		allFilesRead++;
	}
	closedir(dir);

	return simlirarFiles;
}

int main() {

	const char *filePath;
	string input;
	cout << "Input root folder address: ";
	cin >> input;
	cin.get();
	input +="\\";
	filePath = input.c_str();

	int copyFoldersNumber = 0;

	string add[100];
	for (int i = 0; i < 100; i++) {
           // string tempsss = to_string(1);
           stringstream adda ;
           adda<< filePath << i;

		add[i] = adda.str();
		if (list_dir(add[i])) {
			cout << i << " -> is copy file exist: YES" << endl;
			copyFoldersNumber++;
		}
		else
			cout << i << " -> is copy file exist: NO" << endl;
	}
	cout<<"**************************************\n";
	cout<<"in "<<copyFoldersNumber<<" folder copy file exist.(number of all files: "<<allFilesRead<<")"<<endl;
	cout<<"**************************************\n";
	system("pause");
	return 0;
}
